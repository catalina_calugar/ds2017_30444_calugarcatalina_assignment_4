import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;

@Configuration
public class ClientConfig extends WsConfigurerAdapter {
	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath("generated");
		return marshaller;
	}

	@Bean
	public UserClient userClient(Jaxb2Marshaller marshaller) {
		UserClient client = new UserClient();
		client.setDefaultUri("http://localhost:8080/service/userDetailsWsdl.wsdl");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}

	@Bean
	public PackageClient packageClient(Jaxb2Marshaller marshaller) {
		PackageClient client = new PackageClient();
		client.setDefaultUri("http://localhost:8080/service/userDetailsWsdl.wsdl");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}

	@Bean
	public RouteClient routeClient(Jaxb2Marshaller marshaller) {
		RouteClient client = new RouteClient();
		client.setDefaultUri("http://localhost:8080/service/userDetailsWsdl.wsdl");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}

}