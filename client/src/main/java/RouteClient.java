/**
 * Created by cata_ on 12/13/2017.
 */
import generated.*;
import generated.Package;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

/**
 * Created by cata_ on 12/7/2017.
 */
public class RouteClient extends WebServiceGatewaySupport {

    public RouteDetailsResponse getRouteByPackName(String name) {
        RouteDetailsRequest request = new RouteDetailsRequest();
        request.setName(name);
        RouteDetailsResponse response = (RouteDetailsResponse) getWebServiceTemplate().marshalSendAndReceive(
                request, new SoapActionCallback("http://localhost:8080/howtodoinjava/RouteDetailsRequest")
        );
        return response;
    }

    public RouteAddResponse addRoute(Route route) {
        RouteAddRequest request = new RouteAddRequest();
        request.setRoute(route);
        RouteAddResponse response = (RouteAddResponse) getWebServiceTemplate().marshalSendAndReceive(
                request, new SoapActionCallback("http://localhost:8080/howtodoinjava/RouteAddRequest")
        );
        return response;
    }

}
