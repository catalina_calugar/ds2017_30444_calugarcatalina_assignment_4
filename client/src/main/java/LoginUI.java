import generated.User;
import generated.UserAddDeleteResponse;
import generated.UserDetailsResponse;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LoginUI {

    public static JFrame loginFrame;
    private JTextField usernameTextField;
    private JTextField passTextField;
    private UserClient userClient;
    private JLabel errorLabel;
    private PackageClient packageClient;
    private RouteClient routeClient;
    public static String loggedUsername;

    public LoginUI(UserClient userClient, PackageClient packageClient, RouteClient routeClient) {
        this.userClient = userClient;
        this.packageClient = packageClient;
        this.routeClient = routeClient;
        initialize();
        loginFrame.setLocationRelativeTo(null);
        loginFrame.setVisible(true);
    }

    private void initialize() {
        loginFrame = new JFrame();
        loginFrame.getContentPane().setBackground(Color.ORANGE);
        loginFrame.setBounds(0, -30, 508, 321);
        loginFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        loginFrame.getContentPane().setLayout(null);

        JLabel lblUsername = new JLabel("Username:");
        lblUsername.setBounds(31, 98, 92, 16);
        loginFrame.getContentPane().add(lblUsername);

        usernameTextField = new JTextField();
        usernameTextField.setBounds(102, 95, 332, 22);
        loginFrame.getContentPane().add(usernameTextField);
        usernameTextField.setColumns(10);

        JLabel lblPassword = new JLabel("Password:");
        lblPassword.setBounds(31, 147, 71, 16);
        loginFrame.getContentPane().add(lblPassword);

        passTextField = new JTextField();
        passTextField.setBounds(102, 144, 332, 22);
        loginFrame.getContentPane().add(passTextField);
        passTextField.setColumns(10);

        JLabel lblLogin = new JLabel("LOGIN");
        lblLogin.setFont(new Font("Tahoma", Font.PLAIN, 36));
        lblLogin.setBounds(193, 41, 115, 33);
        loginFrame.getContentPane().add(lblLogin);

        JButton btnLogin = new JButton("LoginUI");
        btnLogin.setBounds(102, 208, 160, 41);

        btnLogin.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                UserDetailsResponse response = userClient.getUserByUsername(usernameTextField.getText());
                System.out.println("GET USER BY NAME: " + "Name: " + response.getUser().getUsername() + "Pass: " + response.getUser().getPassword() + "Type: " + response.getUser().getType());

                if (response.getUser().getPassword().equals(passTextField.getText())) {
                    loggedUsername = usernameTextField.getText();
                    if (usernameTextField.getText().equals("admin")) {
                        new AdminUI(packageClient,routeClient);
                        usernameTextField.setText("");
                        passTextField.setText("");
                        loginFrame.setVisible(false);

                    } else {
                        new UserUi(packageClient,routeClient);
                        usernameTextField.setText("");
                        passTextField.setText("");
                        loginFrame.setVisible(false);
                    }
                } else {
                    errorLabel.setText("Username or password invalid! Retry...");
                }
            }
        });

        loginFrame.getContentPane().add(btnLogin);


        JButton btnRegister = new JButton("Register");
        btnRegister.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                //REGISTER
                User user = new User();
                user.setType("normal");
                user.setPassword(passTextField.getText());
                user.setUsername(usernameTextField.getText());
                loggedUsername = usernameTextField.getText();

                UserAddDeleteResponse response = userClient.addUser(user);
                System.out.println("User added: "+response.getMessage());
                errorLabel.setText(response.getMessage());

                if(!response.getMessage().equals("Error!")) {
                    new UserUi(packageClient,routeClient);
                    loginFrame.setVisible(false);
                }
            }
        });
        btnRegister.setBounds(277, 208, 160, 41);
        loginFrame.getContentPane().add(btnRegister);

        errorLabel = new JLabel("");
        errorLabel.setForeground(Color.RED);
        errorLabel.setBounds(230, 179, 200, 20);
        loginFrame.getContentPane().add(errorLabel);
    }
}
