
package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="trackMessageResponse" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "trackMessageResponse"
})
@XmlRootElement(name = "PackageSetForTrackingResponse", namespace = "http://www.howtodoinjava.com/xml/school")
public class PackageSetForTrackingResponse {

    @XmlElement(namespace = "http://www.howtodoinjava.com/xml/school", required = true)
    protected String trackMessageResponse;

    /**
     * Gets the value of the trackMessageResponse property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrackMessageResponse() {
        return trackMessageResponse;
    }

    /**
     * Sets the value of the trackMessageResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrackMessageResponse(String value) {
        this.trackMessageResponse = value;
    }

}
