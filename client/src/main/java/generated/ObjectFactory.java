
package generated;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RouteDetailsResponse }
     * 
     */
    public RouteDetailsResponse createRouteDetailsResponse() {
        return new RouteDetailsResponse();
    }

    /**
     * Create an instance of {@link Route }
     * 
     */
    public Route createRoute() {
        return new Route();
    }

    /**
     * Create an instance of {@link GetAllPackagesResponse }
     * 
     */
    public GetAllPackagesResponse createGetAllPackagesResponse() {
        return new GetAllPackagesResponse();
    }

    /**
     * Create an instance of {@link Package }
     * 
     */
    public Package createPackage() {
        return new Package();
    }

    /**
     * Create an instance of {@link RouteAddResponse }
     * 
     */
    public RouteAddResponse createRouteAddResponse() {
        return new RouteAddResponse();
    }

    /**
     * Create an instance of {@link PackageDeleteResponse }
     * 
     */
    public PackageDeleteResponse createPackageDeleteResponse() {
        return new PackageDeleteResponse();
    }

    /**
     * Create an instance of {@link PackageAddRequest }
     * 
     */
    public PackageAddRequest createPackageAddRequest() {
        return new PackageAddRequest();
    }

    /**
     * Create an instance of {@link PackageAddResponse }
     * 
     */
    public PackageAddResponse createPackageAddResponse() {
        return new PackageAddResponse();
    }

    /**
     * Create an instance of {@link PackageDeleteRequest }
     * 
     */
    public PackageDeleteRequest createPackageDeleteRequest() {
        return new PackageDeleteRequest();
    }

    /**
     * Create an instance of {@link UserDetailsRequest }
     * 
     */
    public UserDetailsRequest createUserDetailsRequest() {
        return new UserDetailsRequest();
    }

    /**
     * Create an instance of {@link GetAllPackagesRequest }
     * 
     */
    public GetAllPackagesRequest createGetAllPackagesRequest() {
        return new GetAllPackagesRequest();
    }

    /**
     * Create an instance of {@link RouteDetailsRequest }
     * 
     */
    public RouteDetailsRequest createRouteDetailsRequest() {
        return new RouteDetailsRequest();
    }

    /**
     * Create an instance of {@link PackageSetForTrackingRequest }
     * 
     */
    public PackageSetForTrackingRequest createPackageSetForTrackingRequest() {
        return new PackageSetForTrackingRequest();
    }

    /**
     * Create an instance of {@link PackageSetForTrackingResponse }
     * 
     */
    public PackageSetForTrackingResponse createPackageSetForTrackingResponse() {
        return new PackageSetForTrackingResponse();
    }

    /**
     * Create an instance of {@link UserAddDeleteRequest }
     * 
     */
    public UserAddDeleteRequest createUserAddDeleteRequest() {
        return new UserAddDeleteRequest();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link PackageDetailsRequest }
     * 
     */
    public PackageDetailsRequest createPackageDetailsRequest() {
        return new PackageDetailsRequest();
    }

    /**
     * Create an instance of {@link PackageDetailsResponse }
     * 
     */
    public PackageDetailsResponse createPackageDetailsResponse() {
        return new PackageDetailsResponse();
    }

    /**
     * Create an instance of {@link RouteAddRequest }
     * 
     */
    public RouteAddRequest createRouteAddRequest() {
        return new RouteAddRequest();
    }

    /**
     * Create an instance of {@link UserDetailsResponse }
     * 
     */
    public UserDetailsResponse createUserDetailsResponse() {
        return new UserDetailsResponse();
    }

    /**
     * Create an instance of {@link UserAddDeleteResponse }
     * 
     */
    public UserAddDeleteResponse createUserAddDeleteResponse() {
        return new UserAddDeleteResponse();
    }

}
