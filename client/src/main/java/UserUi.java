import generated.*;
import generated.Package;

import java.awt.*;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.*;
import javax.swing.table.DefaultTableModel;

public class UserUi {

    private JFrame frame;
    private JTable showPackUserTabel;
    private JTextField searchPackByNameTextField;
    private PackageClient packageClient;
    private RouteClient routeClient;

    public UserUi(PackageClient packageClient, RouteClient routeClient) {
        this.packageClient = packageClient;
        this.routeClient = routeClient;
        initialize();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private void initialize() {
        frame = new JFrame();
        frame.getContentPane().setBackground(Color.ORANGE);
        frame.setBounds(100, 100, 753, 449);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        String[] columns = {"Name", "SendeID", "ReceiverID", "SenderCity", "ReceiverCity", "Tracking"};
        DefaultTableModel tableModel = new DefaultTableModel(columns, 0);
        ;
        showPackUserTabel = new JTable(tableModel);
        showPackUserTabel.setBackground(new Color(255, 255, 224));
        showPackUserTabel.setBounds(16, 39, 390, 312);
        frame.getContentPane().add(showPackUserTabel);

        JButton btnShowPack = new JButton("Show packages");
        btnShowPack.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                //Show all packages for user
                GetAllPackagesResponse response = packageClient.getAllPackagesForUser(LoginUI.loggedUsername);
                java.util.List<Package> packageList = new ArrayList<>();

                packageList = response.getGetAllPackages();
                System.out.print(packageList.size() + " " + packageList.get(0).getName());
                if (packageList != null) {
                    setTabel(tableModel, packageList);
                }
            }
        });
        btnShowPack.setBounds(434, 24, 262, 44);
        frame.getContentPane().add(btnShowPack);

        JLabel lblSearchAllPackages = new JLabel("Search package Name:");
        lblSearchAllPackages.setBounds(434, 91, 171, 16);
        frame.getContentPane().add(lblSearchAllPackages);

        searchPackByNameTextField = new JTextField();
        searchPackByNameTextField.setBackground(new Color(255, 255, 224));
        searchPackByNameTextField.setBounds(434, 112, 262, 22);
        frame.getContentPane().add(searchPackByNameTextField);
        searchPackByNameTextField.setColumns(10);

        JTextArea resultSearchPackageByIdTextArea = new JTextArea();
        resultSearchPackageByIdTextArea.setBackground(new Color(255, 255, 224));
        resultSearchPackageByIdTextArea.setBounds(434, 223, 262, 166);

        JButton btnSearchPackage = new JButton("Search package by name");
        btnSearchPackage.setBounds(434, 147, 262, 25);
        btnSearchPackage.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //search pack by name
                String packageName = searchPackByNameTextField.getText();

                if (packageName != null && packageName != "") {
                    PackageDetailsResponse response = packageClient.getPackageByPackageName(packageName);
                    if (response.getPackage() != null) {
                        System.out.println("Package found: " + response.getPackage().getName() + " from send city: " + response.getPackage().getSenderCity());
                        if (response.getPackage().getSenderUsername().equals(LoginUI.loggedUsername)) {
                            resultSearchPackageByIdTextArea.setText("");
                            resultSearchPackageByIdTextArea.setText("Package found: \n" + "Name: " + response.getPackage().getName() +
                                    "\nSenderUsername: " + response.getPackage().getSenderUsername() + "\nReceiverUsername: " + response.getPackage().getReceiverUsername() +
                                    "\nDestination city: " + response.getPackage().getDestinationCity());
                        } else {
                            resultSearchPackageByIdTextArea.setText("You have no package with this name!");
                        }
                    } else {
                        resultSearchPackageByIdTextArea.setText("You have no package with this name!");
                    }
                } else {
                    JOptionPane.showMessageDialog(frame, "Please input a valid package name !");

                }
            }
        });
        frame.getContentPane().add(btnSearchPackage);

        frame.getContentPane().add(resultSearchPackageByIdTextArea);

        JButton button = new JButton("Logout");
        button.setBounds(12, 364, 97, 25);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.setVisible(false);
                LoginUI.loginFrame.setVisible(true);
            }
        });
        frame.getContentPane().add(button);

        JLabel lblNewLabel = new JLabel("Logged user: " + LoginUI.loggedUsername);
        lblNewLabel.setBounds(105, 10, 211, 16);
        frame.getContentPane().add(lblNewLabel);

        JButton btnSeePackageStatus = new JButton("See package status");
        btnSeePackageStatus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                //See package status
                String packageName = searchPackByNameTextField.getText();

                if (packageName != null && packageName != "") {
                    RouteDetailsResponse response = routeClient.getRouteByPackName(packageName);

                    if (response.getRoute().size() != 0) {
                        java.util.List<Route> routeList = response.getRoute();
                        resultSearchPackageByIdTextArea.setText("");
                        for (Route r : routeList) {
                            resultSearchPackageByIdTextArea.setText(resultSearchPackageByIdTextArea.getText() + r.getPackageName() + " " + r.getCity() + " " + r.getDateTime() + "\n");
                        }
                    } else {
                        resultSearchPackageByIdTextArea.setText("No routes found!");
                    }

                } else {
                    JOptionPane.showMessageDialog(frame, "Please input a valid package name !");
                }
            }
        });
        btnSeePackageStatus.setBounds(434, 185, 262, 25);
        frame.getContentPane().add(btnSeePackageStatus);
    }

    public void setTabel(DefaultTableModel tableModel, java.util.List<Package> packageList) {

        int rowCount = tableModel.getRowCount();
        //Remove rows one by one from the end of the table
        for (int i = rowCount - 1; i >= 0; i--) {
            tableModel.removeRow(i);
        }

        for (int i = 0; i < packageList.size(); i++) {
            String name = packageList.get(i).getName();
            String sendeId = packageList.get(i).getSenderUsername();
            String receiverId = packageList.get(i).getReceiverUsername();
            String senderCity = packageList.get(i).getSenderCity();
            String destCity = packageList.get(i).getDestinationCity();
            boolean tracking = packageList.get(i).isTracking();

            Object[] row = {name, sendeId, receiverId, senderCity, destCity, tracking};
            tableModel.addRow(row);
        }
    }
}
