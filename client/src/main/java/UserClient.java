/**
 * Created by cata_ on 12/13/2017.
 */
import generated.*;
import generated.Package;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

/**
 * Created by cata_ on 12/7/2017.
 */
public class UserClient extends WebServiceGatewaySupport {

    public UserDetailsResponse getUserByUsername(String name) {
        UserDetailsRequest request = new UserDetailsRequest();
        request.setName(name);
        UserDetailsResponse response = (UserDetailsResponse) getWebServiceTemplate().marshalSendAndReceive(
                request, new SoapActionCallback("http://localhost:8080/howtodoinjava/UserDetailsRequest")
        );
        return response;
    }

    public UserAddDeleteResponse addUser(User user) {
        UserAddDeleteRequest request = new UserAddDeleteRequest();
        request.setUser(user);
        UserAddDeleteResponse response = (UserAddDeleteResponse) getWebServiceTemplate().marshalSendAndReceive(
                request, new SoapActionCallback("http://localhost:8080/howtodoinjava/UserAddDeleteRequest")
        );
        return response;
    }

}
