/**
 * Created by cata_ on 12/13/2017.
 */


import generated.*;
import generated.Package;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static java.lang.System.currentTimeMillis;

public class RunSoapClient {
    public static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
    public static Date date = new Date();

    public static void main(String[] args) throws DatatypeConfigurationException {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.register(ClientConfig.class);
        ctx.refresh();

        UserClient userClient = ctx.getBean(UserClient.class);
        PackageClient packageClient = ctx.getBean(PackageClient.class);
        RouteClient routeClient = ctx.getBean(RouteClient.class);

        LoginUI window = new LoginUI(userClient,packageClient,routeClient);

    }
}
