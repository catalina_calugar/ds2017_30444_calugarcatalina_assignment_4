import com.sun.xml.internal.bind.v2.runtime.reflect.Lister;
import generated.*;
import generated.Package;

import java.awt.*;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.Timestamp;
import java.util.*;
import javax.swing.table.DefaultTableModel;

public class AdminUI {

    private JFrame frame;
    private JTextField addCityTextField;
    private JTextField addTimeTextField;
    private JTable showPackagesTable;
    private JButton logoutButton;
    private PackageClient packageClient;
    private RouteClient routeClient;
    private JTextField addPNameTextField;
    private JTextField addPSenderUsernameTextField;
    private JTextField addPReceivUsernameTextField;
    private JTextField addPSendCityTextField;
    private JTextField addPDestCityTextField;

    public AdminUI(PackageClient packageClient, RouteClient routeClient) {
        this.packageClient = packageClient;
        this.routeClient = routeClient;
        initialize();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private void initialize() {
        frame = new JFrame();
        frame.getContentPane().setBackground(Color.ORANGE);
        frame.setBounds(100, 100, 840, 512);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        String[] columns = {"Name", "SendeID", "ReceiverID", "SenderCity", "ReceiverCity", "Tracking"};
        DefaultTableModel tableModel = new DefaultTableModel(columns, 0);
        ;
        showPackagesTable = new JTable(tableModel);
        showPackagesTable.setBackground(new Color(255, 255, 224));
        showPackagesTable.setBounds(16, 44, 356, 357);

        setTabel(tableModel);

        JButton btnAddPackage = new JButton("ADD package");
        btnAddPackage.setBounds(401, 172, 354, 39);
        btnAddPackage.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                //Add package
                Package packToAdd = new Package();
                packToAdd.setName(addPNameTextField.getText());
                packToAdd.setDestinationCity(addPDestCityTextField.getText());
                packToAdd.setSenderCity(addPSendCityTextField.getText());
                packToAdd.setReceiverUsername(addPReceivUsernameTextField.getText());
                packToAdd.setSenderUsername(addPSenderUsernameTextField.getText());
                packToAdd.setTracking(false);

                if (packToAdd != null) {
                    PackageAddResponse packageAddResponse = packageClient.addPackage(packToAdd);
                    System.out.println(packageAddResponse.getAddMessageResponse());

                    if (!packageAddResponse.getAddMessageResponse().equals("Error add package!")) {
                        setTabel(tableModel);
                    } else {
                        JOptionPane.showMessageDialog(frame, "Error add package!");
                    }
                } else {
                    JOptionPane.showMessageDialog(frame, "Invalid package input!");

                }

            }
        });
        frame.getContentPane().add(btnAddPackage);

        JButton btnDeletePackage = new JButton("DELETE package");
        btnDeletePackage.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
            }
        });
        btnDeletePackage.setBounds(401, 235, 354, 32);
        btnDeletePackage.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                //Delete pack
                int selection = showPackagesTable.getSelectedRow();
                String name = showPackagesTable.getValueAt(selection, 0).toString();

                if (name != null) {
                    PackageDeleteResponse packageAddResponse = packageClient.deletePackage(name);
                    System.out.println(packageAddResponse.getDeleteMessageResponse());
                    if (!packageAddResponse.getDeleteMessageResponse().equals("Error delete package!")) {
                        setTabel(tableModel);
                    }
                } else {
                    JOptionPane.showMessageDialog(frame, "Please select a package!");

                }

            }
        });
        frame.getContentPane().add(btnDeletePackage);

        JButton btnRegisterPackage = new JButton("Register package for tracking");
        btnRegisterPackage.setBounds(401, 280, 354, 32);
        btnRegisterPackage.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                //Register pack to tracking
                int selection = showPackagesTable.getSelectedRow();
                String selectedPackName = showPackagesTable.getValueAt(selection, 0).toString();

                if (selectedPackName != null && selectedPackName != "") {
                    PackageSetForTrackingResponse setForTrackingResponse = packageClient.setTackingForPackage(selectedPackName);
                    System.out.println(setForTrackingResponse.getTrackMessageResponse());
                    setTabel(tableModel);
                    JOptionPane.showMessageDialog(frame, setForTrackingResponse.getTrackMessageResponse());
                } else {
                    JOptionPane.showMessageDialog(frame, "Please select a package!");
                }
            }
        });
        frame.getContentPane().add(btnRegisterPackage);

        JButton btnUpdatePackSts = new JButton("Package status updating");
        btnUpdatePackSts.setBounds(401, 386, 354, 39);
        btnUpdatePackSts.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                //UpdatePackSts
                int selection = showPackagesTable.getSelectedRow();
                String selectedPackName = showPackagesTable.getValueAt(selection, 0).toString();

                if (selectedPackName != null && selectedPackName != "") {
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    Route route = new Route();
                    route.setCity(addCityTextField.getText());
                    route.setDateTime(timestamp.toString());
                    route.setPackageName(selectedPackName);
                    route.setOrderNr(0);

                    if (route != null) {
                        RouteAddResponse routeAddResponse = routeClient.addRoute(route);
                        System.out.println(routeAddResponse.getAddRouteResponseMessage());
                        setTabel(tableModel);
                        JOptionPane.showMessageDialog(frame, routeAddResponse.getAddRouteResponseMessage());
                    }
                } else {
                    JOptionPane.showMessageDialog(frame, "Please select a package!");

                }
            }
        });
        frame.getContentPane().add(btnUpdatePackSts);

        JLabel lblAddCity = new JLabel("Add city:");
        lblAddCity.setBounds(401, 358, 56, 16);
        frame.getContentPane().add(lblAddCity);

        addCityTextField = new JTextField();
        addCityTextField.setBackground(new Color(255, 255, 224));
        addCityTextField.setBounds(456, 355, 299, 22);
        frame.getContentPane().add(addCityTextField);
        addCityTextField.setColumns(10);

        frame.getContentPane().add(showPackagesTable);

        JButton btnLogout = new JButton("Logout");
        btnLogout.setBounds(24, 427, 97, 25);
        btnLogout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.setVisible(false);
                LoginUI.loginFrame.setVisible(true);
            }
        });
        frame.getContentPane().add(btnLogout);

        JLabel lblAddPackageName = new JLabel("Add package name:");
        lblAddPackageName.setBounds(401, 27, 124, 16);
        frame.getContentPane().add(lblAddPackageName);

        addPNameTextField = new JTextField();
        addPNameTextField.setBackground(new Color(255, 255, 224));
        addPNameTextField.setBounds(526, 24, 229, 22);
        frame.getContentPane().add(addPNameTextField);
        addPNameTextField.setColumns(10);

        JLabel lblAddSenderUsername = new JLabel("Add sender username:");
        lblAddSenderUsername.setBounds(401, 56, 112, 16);
        frame.getContentPane().add(lblAddSenderUsername);

        addPSenderUsernameTextField = new JTextField();
        addPSenderUsernameTextField.setBackground(new Color(255, 255, 224));
        addPSenderUsernameTextField.setBounds(525, 53, 230, 22);
        frame.getContentPane().add(addPSenderUsernameTextField);
        addPSenderUsernameTextField.setColumns(10);

        JLabel lblAddReceiverUsername = new JLabel("Add receiver username:");
        lblAddReceiverUsername.setBounds(401, 85, 112, 16);
        frame.getContentPane().add(lblAddReceiverUsername);

        addPReceivUsernameTextField = new JTextField();
        addPReceivUsernameTextField.setBackground(new Color(255, 255, 224));
        addPReceivUsernameTextField.setColumns(10);
        addPReceivUsernameTextField.setBounds(525, 82, 229, 22);
        frame.getContentPane().add(addPReceivUsernameTextField);

        addPSendCityTextField = new JTextField();
        addPSendCityTextField.setBackground(new Color(255, 255, 224));
        addPSendCityTextField.setColumns(10);
        addPSendCityTextField.setBounds(525, 110, 229, 22);
        frame.getContentPane().add(addPSendCityTextField);

        addPDestCityTextField = new JTextField();
        addPDestCityTextField.setBackground(new Color(255, 255, 224));
        addPDestCityTextField.setColumns(10);
        addPDestCityTextField.setBounds(525, 137, 229, 22);
        frame.getContentPane().add(addPDestCityTextField);

        JLabel lblAddSenderCity = new JLabel("Add sender city:");
        lblAddSenderCity.setBounds(401, 114, 97, 16);
        frame.getContentPane().add(lblAddSenderCity);

        JLabel lblAddDestinationCity = new JLabel("Add destination city:");
        lblAddDestinationCity.setBounds(401, 140, 124, 16);
        frame.getContentPane().add(lblAddDestinationCity);

        JLabel lblLoggedUser = new JLabel("Logged user: " + LoginUI.loggedUsername);
        lblLoggedUser.setBounds(72, 15, 247, 16);
        frame.getContentPane().add(lblLoggedUser);
    }

    public void setTabel(DefaultTableModel tableModel) {

        int rowCount = tableModel.getRowCount();
        //Remove rows one by one from the end of the table
        for (int i = rowCount - 1; i >= 0; i--) {
            tableModel.removeRow(i);
        }

        GetAllPackagesResponse response = packageClient.getAllPackages();
        java.util.List<Package> packageList = new ArrayList<>();
        packageList = response.getGetAllPackages();
        System.out.print(packageList.size() + " " + packageList.get(0).getName());

        for (int i = 0; i < packageList.size(); i++) {
            String name = packageList.get(i).getName();
            String sendeId = packageList.get(i).getSenderUsername();
            String receiverId = packageList.get(i).getReceiverUsername();
            String senderCity = packageList.get(i).getSenderCity();
            String destCity = packageList.get(i).getDestinationCity();
            boolean tracking = packageList.get(i).isTracking();

            Object[] row = {name, sendeId, receiverId, senderCity, destCity, tracking};
            tableModel.addRow(row);
        }
    }
}
