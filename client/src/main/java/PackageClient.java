/**
 * Created by cata_ on 12/13/2017.
 */
import generated.*;
import generated.Package;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

/**
 * Created by cata_ on 12/7/2017.
 */
public class PackageClient extends WebServiceGatewaySupport {

    public PackageDetailsResponse getPackageByPackageName(String name) {
        PackageDetailsRequest request = new PackageDetailsRequest();
        request.setName(name);
        PackageDetailsResponse response = (PackageDetailsResponse) getWebServiceTemplate().marshalSendAndReceive(
                request, new SoapActionCallback("http://localhost:8080/howtodoinjava/PackageDetailsResponse")
        );
        return response;
    }

    public PackageAddResponse addPackage(Package pack) {
        PackageAddRequest request = new PackageAddRequest();
        request.setPackage(pack);
        PackageAddResponse response = (PackageAddResponse) getWebServiceTemplate().marshalSendAndReceive(
                request, new SoapActionCallback("http://localhost:8080/howtodoinjava/PackageAddRequest")
        );
        return response;
    }

    public PackageDeleteResponse deletePackage(String name) {
        PackageDeleteRequest request = new PackageDeleteRequest();
        request.setPackageName(name);
        PackageDeleteResponse response = (PackageDeleteResponse) getWebServiceTemplate().marshalSendAndReceive(
                request, new SoapActionCallback("http://localhost:8080/howtodoinjava/PackageDeleteRequest")
        );
        return response;
    }

    public PackageSetForTrackingResponse setTackingForPackage(String name) {
        PackageSetForTrackingRequest request = new PackageSetForTrackingRequest();
        request.setPackageName(name);
        PackageSetForTrackingResponse response = (PackageSetForTrackingResponse) getWebServiceTemplate().marshalSendAndReceive(
                request, new SoapActionCallback("http://localhost:8080/howtodoinjava/PackageSetForTrackingRequest")
        );
        return response;
    }

    public GetAllPackagesResponse getAllPackages() {
        GetAllPackagesRequest request = new GetAllPackagesRequest();
        request.setMessage("GET_PACKS");
        GetAllPackagesResponse response = (GetAllPackagesResponse) getWebServiceTemplate().marshalSendAndReceive(
                request, new SoapActionCallback("http://localhost:8080/howtodoinjava/GetAllPackagesRequest")
        );
        return response;
    }

    public GetAllPackagesResponse getAllPackagesForUser(String username) {
        GetAllPackagesRequest request = new GetAllPackagesRequest();
        request.setMessage(username);
        GetAllPackagesResponse response = (GetAllPackagesResponse) getWebServiceTemplate().marshalSendAndReceive(
                request, new SoapActionCallback("http://localhost:8080/howtodoinjava/GetAllPackagesRequest")
        );
        return response;
    }
}
