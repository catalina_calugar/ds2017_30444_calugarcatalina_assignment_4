package model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by cata_ on 12/6/2017.
 */

@Entity
@Table(name = "route", catalog = "assign4")
public class Route implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idroute")
    private int idroute;
    @Column(name = "packageId")
    private int packageId;
    @Column(name = "city")
    private String city;
    @Column(name = "dateTime")
    private Timestamp dateTime;
    @Column(name = "orderNr")
    private int orderNr;

    public Route() {
    }

    public Route(int packageId, String city, Timestamp dateTime, int orderNr) {
        this.packageId = packageId;
        this.city = city;
        this.dateTime = dateTime;
        this.orderNr = orderNr;
    }

    public int getIdroute() {
        return idroute;
    }

    public void setIdroute(int idroute) {
        this.idroute = idroute;
    }

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Timestamp getDateTime() {
        return dateTime;
    }

    public void setDateTime(Timestamp dateTime) {
        this.dateTime = dateTime;
    }

    public int getOrderNr() {
        return orderNr;
    }

    public void setOrderNr(int orderNr) {
        this.orderNr = orderNr;
    }
}
