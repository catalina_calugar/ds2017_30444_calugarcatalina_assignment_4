package model;

import javax.persistence.*;

/**
 * Created by cata_ on 12/6/2017.
 */

@Entity
@Table(name = "package", catalog = "assign4")
public class Package implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpackage")
    private int idpackage;
    @Column(name = "senderId")
    private int senderId;
    @Column(name = "receiverId")
    private int receiverId;
    @Column(name = "name")
    private String name;
    @Column(name = "senderCity")
    private String senderCity;
    @Column(name = "destinationCity")
    private String destinationCity;
    @Column(name = "tracking")
    private boolean tracking;

    public Package() {
    }

    public Package(int senderId, int receiverId, String name, String senderCity, String destinationCity, boolean tracking) {
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.name = name;
        this.senderCity = senderCity;
        this.destinationCity = destinationCity;
        this.tracking = tracking;
    }

    public int getIdpackage() {
        return idpackage;
    }

    public Package(int senderId, int receiverId, String name, String senderCity, String destinationCity) {
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.name = name;
        this.senderCity = senderCity;
        this.destinationCity = destinationCity;
    }

    public void setIdpackage(int idpackage) {
        this.idpackage = idpackage;
    }

    public int getSenderId() {
        return senderId;
    }

    public void setSenderId(int senderId) {
        this.senderId = senderId;
    }

    public int getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(int receiverId) {
        this.receiverId = receiverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSenderCity() {
        return senderCity;
    }

    public void setSenderCity(String senderCity) {
        this.senderCity = senderCity;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    public boolean isTracking() {
        return tracking;
    }

    public void setTracking(boolean tracking) {
        this.tracking = tracking;
    }

    @Override
    public String toString() {
        return "Package{" +
                "idpackage=" + idpackage +
                ", senderId=" + senderId +
                ", receiverId=" + receiverId +
                ", name='" + name + '\'' +
                ", senderCity='" + senderCity + '\'' +
                ", destinationCity='" + destinationCity + '\'' +
                ", tracking=" + tracking +
                '}';
    }
}
