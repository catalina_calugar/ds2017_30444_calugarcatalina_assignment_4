package com.example.howtodoinjava.springbootsoapservice;

import com.howtodoinjava.xml.school.UserDetailsRequest;
import com.howtodoinjava.xml.school.UserDetailsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class UserEndpoint {
	private static final String NAMESPACE_URI = "http://www.howtodoinjava.com/xml/school";

	private UserRepository UserRepository;

	@Autowired
	public UserEndpoint(UserRepository UserRepository) {
		this.UserRepository = UserRepository;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "UserDetailsRequest")
	@ResponsePayload
	public UserDetailsResponse getUser(@RequestPayload UserDetailsRequest request) {
		UserDetailsResponse response = new UserDetailsResponse();
		response.setUser(UserRepository.findUser(request.getName()));

		return response;
	}
}