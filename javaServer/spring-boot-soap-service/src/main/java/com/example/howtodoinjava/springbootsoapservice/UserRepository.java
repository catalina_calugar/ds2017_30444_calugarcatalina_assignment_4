package com.example.howtodoinjava.springbootsoapservice;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import com.howtodoinjava.xml.school.User;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class UserRepository {
	private static final Map<String, User> users = new HashMap<>();

	@PostConstruct
	public void initData() {
		
		User user = new User();
		user.setUsername("Sajal");
		user.setPassword("hfhd");
		user.setType("Pune");
		users.put(user.getUsername(), user);

	}

	public User findUser(String name) {
		Assert.notNull(name, "The User's name must not be null");
		return users.get(name);
	}
}