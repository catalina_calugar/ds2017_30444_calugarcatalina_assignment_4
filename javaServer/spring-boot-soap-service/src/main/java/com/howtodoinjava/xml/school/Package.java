
package com.howtodoinjava.xml.school;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Package complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Package">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idpackage" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="senderId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="receiverId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="senderCity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="destinationCity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tracking" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Package", namespace = "http://www.howtodoinjava.com/xml/school", propOrder = {
    "name",
    "idpackage",
    "senderId",
    "receiverId",
    "senderCity",
    "destinationCity",
    "tracking"
})
public class Package {

    @XmlElement(namespace = "http://www.howtodoinjava.com/xml/school", required = true)
    protected String name;
    @XmlElement(namespace = "http://www.howtodoinjava.com/xml/school")
    protected int idpackage;
    @XmlElement(namespace = "http://www.howtodoinjava.com/xml/school")
    protected int senderId;
    @XmlElement(namespace = "http://www.howtodoinjava.com/xml/school")
    protected int receiverId;
    @XmlElement(namespace = "http://www.howtodoinjava.com/xml/school", required = true)
    protected String senderCity;
    @XmlElement(namespace = "http://www.howtodoinjava.com/xml/school", required = true)
    protected String destinationCity;
    @XmlElement(namespace = "http://www.howtodoinjava.com/xml/school")
    protected boolean tracking;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the idpackage property.
     * 
     */
    public int getIdpackage() {
        return idpackage;
    }

    /**
     * Sets the value of the idpackage property.
     * 
     */
    public void setIdpackage(int value) {
        this.idpackage = value;
    }

    /**
     * Gets the value of the senderId property.
     * 
     */
    public int getSenderId() {
        return senderId;
    }

    /**
     * Sets the value of the senderId property.
     * 
     */
    public void setSenderId(int value) {
        this.senderId = value;
    }

    /**
     * Gets the value of the receiverId property.
     * 
     */
    public int getReceiverId() {
        return receiverId;
    }

    /**
     * Sets the value of the receiverId property.
     * 
     */
    public void setReceiverId(int value) {
        this.receiverId = value;
    }

    /**
     * Gets the value of the senderCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderCity() {
        return senderCity;
    }

    /**
     * Sets the value of the senderCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderCity(String value) {
        this.senderCity = value;
    }

    /**
     * Gets the value of the destinationCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationCity() {
        return destinationCity;
    }

    /**
     * Sets the value of the destinationCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationCity(String value) {
        this.destinationCity = value;
    }

    /**
     * Gets the value of the tracking property.
     * 
     */
    public boolean isTracking() {
        return tracking;
    }

    /**
     * Sets the value of the tracking property.
     * 
     */
    public void setTracking(boolean value) {
        this.tracking = value;
    }

}
