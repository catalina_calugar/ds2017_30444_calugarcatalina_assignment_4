
package com.howtodoinjava.xml.school;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.howtodoinjava.xml.school package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.howtodoinjava.xml.school
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RouteDetailsResponse }
     * 
     */
    public RouteDetailsResponse createRouteDetailsResponse() {
        return new RouteDetailsResponse();
    }

    /**
     * Create an instance of {@link Route }
     * 
     */
    public Route createRoute() {
        return new Route();
    }

    /**
     * Create an instance of {@link UserDetailsRequest }
     * 
     */
    public UserDetailsRequest createUserDetailsRequest() {
        return new UserDetailsRequest();
    }

    /**
     * Create an instance of {@link RouteDetailsRequest }
     * 
     */
    public RouteDetailsRequest createRouteDetailsRequest() {
        return new RouteDetailsRequest();
    }

    /**
     * Create an instance of {@link PackageDetailsRequest }
     * 
     */
    public PackageDetailsRequest createPackageDetailsRequest() {
        return new PackageDetailsRequest();
    }

    /**
     * Create an instance of {@link PackageDetailsResponse }
     * 
     */
    public PackageDetailsResponse createPackageDetailsResponse() {
        return new PackageDetailsResponse();
    }

    /**
     * Create an instance of {@link Package }
     * 
     */
    public Package createPackage() {
        return new Package();
    }

    /**
     * Create an instance of {@link UserDetailsResponse }
     * 
     */
    public UserDetailsResponse createUserDetailsResponse() {
        return new UserDetailsResponse();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

}
