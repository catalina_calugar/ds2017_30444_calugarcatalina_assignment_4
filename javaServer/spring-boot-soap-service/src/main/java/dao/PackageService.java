package dao;

import model.Package;
import org.hibernate.*;

import java.util.List;

/**
 * Created by cata_ on 10/28/2017.
 */

public class PackageService {
    private SessionFactory factory;

    public PackageService(SessionFactory factory) {
        this.factory = factory;
    }

    @SuppressWarnings("unchecked")
    public Object add(Object o) {
        Package pack = (Package) o;

        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(pack);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }

        return pack == null || session == null ? null : pack;
    }

    @SuppressWarnings("unchecked")
    public Object edit(Object o) {
        Package pack = (Package) o;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(pack);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }

        return pack == null || session == null ? null : pack;
    }

    @SuppressWarnings("unchecked")
    public Object delete(Object o) {
        Package pack = (Package) o;

        if (getObject(pack.getIdpackage()) != null) {
            Session session = factory.openSession();
            Transaction tx = null;
            try {
                tx = session.beginTransaction();
                session.delete(pack);
                tx.commit();
            } catch (HibernateException e) {
                if (tx != null) {
                    tx.rollback();
                }
            } finally {
                session.close();
            }
        }
        return pack == null ? null : pack;
    }

    @SuppressWarnings("unchecked")
    public Object getObject(int id) {
        List<Package> packs = null;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Package WHERE idpackage = :id");
            query.setParameter("id", id);
            packs = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return packs == null || session == null ? null : packs.get(0);
    }

//    @SuppressWarnings("unchecked")
//    public Object getObject(String packname, String password) {
//        List<Package> packs = null;
//        Session session = factory.openSession();
//        Transaction tx = null;
//        try {
//            tx = session.beginTransaction();
//            Query query = session.createQuery("FROM Package WHERE packname = :packname AND password = :password");
//            query.setParameter("packname", packname);
//            query.setParameter("password", password);
//            packs = query.list();
//            tx.commit();
//        } catch (HibernateException e) {
//            if (tx != null) {
//                tx.rollback();
//            }
//        } finally {
//            session.close();
//        }
//        return packs == null || session == null ? null : packs.get(0);
//    }

}
