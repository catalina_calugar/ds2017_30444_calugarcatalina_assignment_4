package dao;

import model.User;
import org.hibernate.*;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by cata_ on 10/28/2017.
 */

@Service
public class UserService {
    private SessionFactory factory;

    public UserService(SessionFactory factory) {
        this.factory = factory;
    }

    @SuppressWarnings("unchecked")
    public Object add(Object o) {
        User user = (User) o;

        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(user);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }

        return user == null || session == null ? null : user;
    }

    @SuppressWarnings("unchecked")
    public Object edit(Object o) {
        User user = (User) o;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(user);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }

        return user == null || session == null ? null : user;
    }

    @SuppressWarnings("unchecked")
    public Object delete(Object o) {
        User user = (User) o;

        if (getObject(user.getIdusers()) != null) {
            Session session = factory.openSession();
            Transaction tx = null;
            try {
                tx = session.beginTransaction();
                session.delete(user);
                tx.commit();
            } catch (HibernateException e) {
                if (tx != null) {
                    tx.rollback();
                }
            } finally {
                session.close();
            }
        }
        return user == null ? null : user;
    }

    @SuppressWarnings("unchecked")
    public Object getObject(int id) {
        List<User> users = null;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM User WHERE idusers = :id");
            query.setParameter("id", id);
            users = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return users == null || session == null ? null : users.get(0);
    }

    @SuppressWarnings("unchecked")
    public Object getObject(String username, String password) {
        List<User> users = null;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM User WHERE username = :username AND password = :password");
            query.setParameter("username", username);
            query.setParameter("password", password);
            users = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return users == null || session == null ? null : users.get(0);
    }

}
