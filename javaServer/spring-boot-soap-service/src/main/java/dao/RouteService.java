package dao;

import model.Route;
import org.hibernate.*;

import java.util.List;

/**
 * Created by cata_ on 12/6/2017.
 */
public class RouteService {
    private SessionFactory factory;

    public RouteService(SessionFactory factory) {
        this.factory = factory;
    }

    @SuppressWarnings("unchecked")
    public Object add(Object o) {
        Route route = (Route) o;

        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(route);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }

        return route == null || session == null ? null : route;
    }

    @SuppressWarnings("unchecked")
    public Object edit(Object o) {
        Route route = (Route) o;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(route);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }

        return route == null || session == null ? null : route;
    }

    @SuppressWarnings("unchecked")
    public Object delete(Object o) {
        Route route = (Route) o;

        if (getObject(route.getIdroute()) != null) {
            Session session = factory.openSession();
            Transaction tx = null;
            try {
                tx = session.beginTransaction();
                session.delete(route);
                tx.commit();
            } catch (HibernateException e) {
                if (tx != null) {
                    tx.rollback();
                }
            } finally {
                session.close();
            }
        }
        return route == null ? null : route;
    }

    @SuppressWarnings("unchecked")
    public Object getObject(int id) {
        List<Route> routes = null;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Route WHERE idroute = :id");
            query.setParameter("id", id);
            routes = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return routes == null || session == null ? null : routes.get(0);
    }

//    @SuppressWarnings("unchecked")
//    public Object getObject(String routename, String password) {
//        List<Route> routes = null;
//        Session session = factory.openSession();
//        Transaction tx = null;
//        try {
//            tx = session.beginTransaction();
//            Query query = session.createQuery("FROM Route WHERE routename = :routename AND password = :password");
//            query.setParameter("routename", routename);
//            query.setParameter("password", password);
//            routes = query.list();
//            tx.commit();
//        } catch (HibernateException e) {
//            if (tx != null) {
//                tx.rollback();
//            }
//        } finally {
//            session.close();
//        }
//        return routes == null || session == null ? null : routes.get(0);
//    }

}
